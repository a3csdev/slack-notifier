<?php
require 'vendor/autoload.php';

use GuzzleHttp\Client;

class SlackNotifier
{

	// guzzle instance config
    private $gclient_config = [
        'verify'          => false
    ];

    // for slack client
	private $slack_settings = [
		'channel' => '#channel-name',
		'username' => 'Notification Title'
	];

	// guzzle instance
	private $guzzle;

	// markdown template path
	private $markdowntpl = 'templates/notification.md';

	// set your slack endpoint url here
	const SLACK_ENDPOINT = 'https://hooks.slack.com/services/XXXXXXXXX/XXXXXXXXX/xxxxxxxxxxxxxxxxxxx';
	
	// define the site which you run this notifier
	const DOMAIN = 'yoursite.com';


	public function __construct()
	{
		// Constructor -> initialize a new guzzle instance
		$this->guzzle = new Client($this->gclient_config);
	}


	public function notify($data)
	{
		// initialize slack client
		$slackclient = new Maknz\Slack\Client(self::SLACK_ENDPOINT, $this->slack_settings, $this->guzzle);

		// read notification template
		try{

			if(!$msgbody = file_get_contents($this->markdowntpl)){
				throw new Exception('Couldn\'t Open Template');
			}

		}catch(Exception $e){
			return false;
		}
      	

		// replace template placeholders with actual data
     	$msgbody = str_replace('{SITENAME}', self::DOMAIN, $msgbody);
      	$msgbody = str_replace('{NAME}', $data['guest_name'], $msgbody);
      	$msgbody = str_replace('{EMAIL}', $data['guest_email'], $msgbody);
      	$msgbody = str_replace('{MESSAGE}', $data['guest_msg'], $msgbody);
      	$msgbody = str_replace('{DATE}', date('Y-m-d'), $msgbody);

      	$slackclient->send($msgbody);

      	return true;

	}
}