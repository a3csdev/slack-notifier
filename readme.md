## Slack Notifier for Web Forms

This class will enable you to send slack notifications from your website forms

##### Usage Instructions

- Clone the repository

	git clone git@gitlab.com:a3csdev/slack-notifier.git

- Install dependencies with composer

	composer install

- Open SlackNotifier.php in a text editor and edit below config options as necessary

	__#channel-name__  (Channel to which notifications are sent to)

	__Notification Title__  (Name to be displayed on Slack)

	__SLACK_ENDPOINT__ (The webhook URL)

	__DOMAIN__ (domain of the site you are integrating this notifier)

- Add below code to the script from which you want to send the notification
```
	require 'SlackNotifier.php';

	// prepare an array with the values submitted from the form
	$data = [
		'guest_name' => 'Guest Name',
		'guest_email' => 'guest@domain',
		'guest_msg' => 'This is a testing message'
	];

	// instantiate notifier
	$slack = new SlackNotifier();

	// send notification
	$slack->notify($data);
```

- Check out tests/send.php to see this in action

Thats it. Thanks.
Amila K.