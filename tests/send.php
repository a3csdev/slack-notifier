<?php
require '../SlackNotifier.php';

$data = [
	'guest_name' => 'Guest Name',
	'guest_email' => 'guest@domain',
	'guest_msg' => 'This is a testing message'
];

$slack = new SlackNotifier();

$slack->notify($data);